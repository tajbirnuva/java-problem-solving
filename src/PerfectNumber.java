import java.util.Scanner;

public class PerfectNumber {
    static void generatePerfectNumber(int l) {
        int counter = 0;
        for (int n = 2; n < l; n++) {
            int sum = 1;
            for (int i = 2; i * i <= n; i++) {
                if (n % i == 0) {
                    if (i * i != n)
                        sum = sum + i + n / i;
                    else
                        sum = sum + i;
                }
            }

            if (sum == n && n != 1 && counter <= 0) {
                System.out.print(n);
                counter++;
            } else if (sum == n && n != 1 && counter > 0) {
                System.out.print(", " + n);
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt();
        generatePerfectNumber(R);
    }
}