import java.util.Scanner;

public class Armstrong {

    static String isArmstrong(int n){

        int originalNumber=n;
        int remainder, result = 0;
        int length=Integer.toString(n).length();;
        while (originalNumber != 0)
        {
            remainder = originalNumber % 10;
            result += Math.pow(remainder, length);
            originalNumber /= 10;
        }

        if(result == n)
            return "Armstrong Number";
        else
            return "Not Armstrong Number";
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int N=sc.nextInt();
        System.out.println(isArmstrong(N));
    }
}